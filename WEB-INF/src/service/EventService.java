package service;

import java.util.List;

import domain.Event;



public interface EventService {
	
	public void addEvent ( Event event );
	
	public List<Event> getAllEvents();
}
